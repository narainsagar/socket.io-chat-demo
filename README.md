
# Socket.IO Chat Demo App

A simple chat demo for socket.io

## How to use

```
$ git clone git@bitbucket.org:narainsagar/socket.io-chat-demo.git
$ cd socket.io-chat
$ npm install
$ node .  (OR) $ node index
```

And point your browser to `http://localhost:3000`. Optionally, specify
a port by supplying the `PORT` env variable.

## Features

- Multiple users can join a chat room by each entering a unique username
on website load.
- Users can type chat messages to the chat room.
- A notification is sent to all users when a user joins or leaves
the chatroom.
- when some type notify all by 'x is typing'
- hide 'is typing' if one stop typing.

Cheers!
